const Joi = require('joi');

const validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .min(3)
        .required()
        .email(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

    role: Joi.string()
        .pattern(new RegExp('^(DRIVER|SHIPPER)$'))
        .required(),
  });
  await schema.validateAsync(req.body);
  next();
};

const validateTruckType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .pattern(new RegExp('^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$'))
        .required(),
  });
  await schema.validateAsync(req.body);
  next();
};

module.exports = {
  validateRegistration,
  validateTruckType,
};
