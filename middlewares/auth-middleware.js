const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const ApiError = require('../errors/api-error');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    throw ApiError.unauthorized('No Authorization http header found!');
  }

  // eslint-disable-next-line camelcase
  const [tokenType, jwt_token] = header.split(' ');

  // eslint-disable-next-line camelcase
  if (!jwt_token) {
    throw ApiError.unauthorized(`No ${tokenType} token found!`);
  }

  req.user = jwt.verify(jwt_token, JWT_SECRET);
  next();
};
