const {User} = require('../models/userModel');
const ApiError = require('../errors/api-error');

const isDriver = async (req, res, next) => {
  const user = await User.findOne({_id: req.user._id, role: 'DRIVER'});
  if (!user) {
    throw ApiError.badRequest('You have to be a driver');
  }
  next();
};

const isShipper = async (req, res, next) => {
  const user = await User.findOne({_id: req.user._id, role: 'SHIPPER'});
  if (!user) {
    throw ApiError.badRequest('You have to be a shipper');
  }
  next();
};

module.exports = {isDriver, isShipper};
