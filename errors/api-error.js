/** Class representing a statusCode */
class ApiError {
  /**
           * @param {number} code - status code
           * @param {string} message - message value
           */
  constructor(code, message) {
    this.code = code;
    this.message = message;
  }
  /**
             * @param {string} message - message value
             * @return {ApiError} new error
             */
  static badRequest(message) {
    return new ApiError(400, message);
  }
  /**
             * @param {string} message - message value
             * @return {ApiError} new error
             */
  static notFound(message) {
    return new ApiError(404, message);
  }
  /**
             * @param {string} message - message value
             *  @return {ApiError} new error
             */
  static unauthorized(message) {
    return new ApiError(401, message);
  }
  /**
             * @param {string} message - message value
             * @return {ApiError} new error
             */
  static serverError(message) {
    return new ApiError(500, message);
  }
}

module.exports = ApiError;
