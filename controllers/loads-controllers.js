const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {getTruckSize} = require('../helpers/getTruckSize');
const ApiError = require('../errors/api-error');


const getLoadsList = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  let loads = [];
  user.role === 'SHIPPER' ?
    loads = await Load.find({created_by: req.user._id}) :
    loads = await Load.find({assigned_to: req.user._id});
  res.status(200).json({loads});
};


const createLoad = async (req, res) => {
  const load = new Load({
    created_by: req.user._id,
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height,
    },
  });
  await load.save();
  res.status(200).json({message: 'Load created successfully'});
};


const deleteLoad = async (req, res) => {
  const load =
    await Load.findOne({
      _id: req.params.id,
      created_by: req.user._id,
      status: 'NEW',
    });
  if (!load) {
    throw ApiError.badRequest(
        `Load with id: ${req.params.id} does not exist or is being delivered`,
    );
  }
  await load.remove();
  res.status(200).json({message: 'Load deleted successfully'});
};


const updateState = async (req, res) => {
  const states = [null,
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'];
  const load = await Load.findOne({
    assigned_to: req.user._id,
    state: {$ne: 'Arrived to delivery'},
  });

  if (!load) {
    throw ApiError.badRequest(`No active loads found`);
  }
  const newLogs = load.logs;
  const currentState = states[states.indexOf(load.state) + 1] > 3 ?
    'Arrived to delivery' :
    states[states.indexOf(load.state) + 1];
  await load.updateOne({state: currentState});

  if (currentState === 'En route to delivery') {
    await load.updateOne({status: 'SHIPPED'});
  }
  if (currentState === 'Arrived to delivery') {
    await Truck.findOneAndUpdate({assigned_to: load.assigned_to, status: 'OL'},
        {status: 'IS'});
  }
  newLogs.push({
    message: `Load state changed to ${currentState}`,
    time: new Date,
  });
  await load.updateOne({logs: newLogs});
  res.status(200).json({message: `Load state changed to ${currentState}`});
};


const getLoadById = async (req, res) => {
  const load = await Load.findOne({
    $and: [{_id: req.params.id},
      {$or: [{created_by: req.user._id}, {assigned_to: req.user._id}]}],
  });
  if (!load) {
    throw ApiError.badRequest(`Load with id: ${req.params.id} does not exist`);
  }
  res.status(200).json({load});
};


const getShippingInfo = async (req, res) => {
  const load = await Load.findOne({
    _id: req.params.id,
    created_by: req.user._id,
  });
  if (!load) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id} does not exist`);
  }
  const truck = (await Truck.findOne({assigned_to: load.assigned_to})) || {};
  res.status(200).json({load, truck});
};


const postLoad = async (req, res) => {
  const load = await Load.findOne({
    _id: req.params.id,
    created_by: req.user._id,
  });
  if (!load) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id} does not exist`);
  }

  const newLogs = load.logs;
  await load.updateOne({status: 'POSTED'});
  const trucks = await Truck.find({assigned_to: {$ne: null}, status: 'IS'});

  newLogs.push({
    message: `Load status changed to 'POSTED'`,
    time: new Date,
  });

  const truck = trucks.filter(
      (el) => getTruckSize(el).payload >= load.payload &&
      getTruckSize(el).width >= load.dimensions.width &&
      getTruckSize(el).length >= load.dimensions.length &&
      getTruckSize(el).height >= load.dimensions.height)[0];

  if (!truck) {
    await load.updateOne({status: 'NEW'});
    newLogs.push({
      message: `Load status changed to 'NEW'`,
      time: new Date,
    });
  } else {
    await truck.updateOne({status: 'OL'});
    newLogs.push({
      message: `Load status changed to 'ASSIGNED'`,
      time: new Date,
    });
    await load.updateOne({
      assigned_to: truck.assigned_to,
      state: 'En route to Pick Up', status: 'ASSIGNED', logs: newLogs,
    });
  }

  res.status(200).
      json({message: 'Load posted successfully', driver_found: !!truck});
};


const getActiveLoad = async (req, res) => {
  const load = (await Load.findOne({
    assigned_to: req.user._id,
    state: {$ne: 'Arrived to delivery'},
  })) || {};
  res.status(200).json({load});
};


const updateLoadInfo = async (req, res) => {
  const load =
    await Load.findOne({
      _id: req.params.id,
      created_by: req.user._id,
      status: 'NEW',
    });
  if (!load) {
    throw ApiError.badRequest(
        `Load with id: ${req.params.id} does not exist or is being delivered`,
    );
  }
  await load.updateOne({
    name: req.body.name || load.name,
    payload: req.body.payload || load.payload,
    pickup_address: req.body.pickup_address || load.pickup_address,
    delivery_address: req.body.delivery_address || load.delivery_address,
    dimensions: {
      width: req.body.dimensions === undefined ? load.dimensions.width :
        req.body.dimensions.width,
      length: req.body.dimensions === undefined ? load.dimensions.length :
        req.body.dimensions.length,
      height: req.body.dimensions === undefined ? load.dimensions.height :
        req.body.dimensions.height,
    },
  });

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports = {
  getLoadsList,
  createLoad,
  deleteLoad,
  updateState,
  getLoadById,
  getShippingInfo,
  postLoad,
  getActiveLoad,
  updateLoadInfo,
};
