const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const ApiError = require('../errors/api-error');

const getProfileInfo = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  console.log(req.user);
  res.status(200).json({
    user: {
      _id: user._id,
      email: user.email,
      created_date: user.created_date,
    },
  });
};

const changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({_id: req.user._id});
  if (!user || !(await bcrypt.compare(oldPassword, user.password))) {
    throw ApiError.badRequest('Wrong userID or old password');
  }
  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});
  res.status(200).json({message: 'Password changed successfully'});
};

const deleteUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id, role: 'SHIPPER'});
  if (!user) {
    throw ApiError.badRequest(`User with id: ${req.params.id} does not exist`);
  }
  await user.deleteOne();
  res.status(200).json({message: 'Profile deleted successfully'});
};


module.exports = {getProfileInfo, deleteUser, changePassword};
