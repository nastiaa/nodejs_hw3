const {Truck} = require('../models/truckModel');
const ApiError = require('../errors/api-error');

const getAllTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id});
  res.status(200).json({trucks});
};

const createNewTruck = async (req, res) => {
  const truck = new Truck({
    created_by: req.user._id,
    type: req.body.type,
  });
  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
};

const getTruckById = async (req, res) => {
  const truck = await Truck.findOne({_id: req.params.id,
    created_by: req.user._id});
  if (!truck) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id} does not exist`);
  }
  res.status(200).json({truck});
};

const updatePayload = async (req, res) => {
  const truck = await Truck.findOne({_id: req.params.id,
    created_by: req.user._id, assigned_to: null});
  if (!truck) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id}
    does not exist or currently assigned`);
  }
  await truck.updateOne({type: req.body.type});
  res.status(200).json({message: 'Truck details changed successfully'});
};

const deleteTruck = async (req, res) => {
  const truck = await Truck.findOne({_id: req.params.id,
    created_by: req.user._id, assigned_to: null});
  if (!truck) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id}
     does not exist or currently assigned`);
  }
  await truck.remove();
  res.status(200).json({message: 'Truck deleted successfully'});
};

const assignTruck = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id});
  if (!trucks.every((truck) => truck.assigned_to === null)) {
    throw ApiError.badRequest('You can only assign one truck at a time');
  }
  const truck = await Truck.findOne({_id: req.params.id,
    created_by: req.user._id});
  if (!truck) {
    throw ApiError.badRequest(`Truck with id: ${req.params.id} does not exist`);
  }
  await truck.updateOne({assigned_to: req.user._id});
  res.status(200).json({message: 'Truck assigned successfully'});
};

module.exports = {getAllTrucks,
  createNewTruck,
  getTruckById,
  updatePayload,
  deleteTruck,
  assignTruck};
