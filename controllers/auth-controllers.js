const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const ApiError = require('../errors/api-error');

const register = async (req, res) => {
  const {email, password, role} = req.body;
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.status(200).json({message: 'User created successfully'});
};

const login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw ApiError.badRequest('Wrong email or password');
  }

  // eslint-disable-next-line camelcase
  const jwt_token = jwt.sign({
    email: user.email,
    _id: user._id,
  }, JWT_SECRET);
  res.status(200).json({jwt_token});
};

module.exports = {login, register};
