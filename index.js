require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const apiErrorHandler = require('./middlewares/error-handle-middleware');
const trucksRouter = require('./routers/trucks-router');
const authRouter = require('./routers/auth-router');
const usersRouter = require('./routers/users-router');
const loadsRouter = require('./routers/loads-router');

const port = process.env.PORT || 8080;

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);
app.use(apiErrorHandler);

const start = async () => {
  await mongoose.connect('mongodb+srv://testUser:testUser23@cluster0.yrlbx.mongodb.net/truckservice?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

app.listen(port, () => {
  console.log('Server has been started...');
});

start();
