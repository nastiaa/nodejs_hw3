const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: { //  [SPRINTER, SMALL STRAIGHT, LARGE STRAIGHT ]
    type: String,
    required: true,
  },
  status: { // [OL,IS]
    type: String,
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
