const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: { // [ NEW, POSTED, ASSIGNED, SHIPPED]
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
      width: Number,
      length: Number,
      height: Number,
    },
    required: true,
  },
  logs: {
    type: [{
      message: String,
      time: Date,
    }],
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
