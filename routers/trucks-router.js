const express = require('express');
const router = new express.Router();
const {getAllTrucks,
  createNewTruck,
  getTruckById,
  updatePayload,
  deleteTruck,
  assignTruck} = require('../controllers/trucks-controllers');
const {isDriver} = require('../middlewares/check-role-middleware');
const {asyncWraper} = require('../helpers/asyncWraper.js');
const {authMiddleware} = require('../middlewares/auth-middleware');
const {validateTruckType} = require('../middlewares/validation-middleware');

router.get('/', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(getAllTrucks));

router.post('/', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(validateTruckType),
    asyncWraper(createNewTruck));

router.get('/:id', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(getTruckById));

router.put('/:id', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(validateTruckType),
    asyncWraper(updatePayload));

router.delete('/:id', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(deleteTruck));

router.post('/:id/assign', authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(assignTruck));

module.exports = router;
