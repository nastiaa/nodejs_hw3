const express = require('express');
const router = new express.Router();
const {isDriver, isShipper} = require('../middlewares/check-role-middleware');
const {getLoadsList,
  createLoad,
  deleteLoad,
  updateState,
  getLoadById,
  getShippingInfo,
  postLoad,
  getActiveLoad,
  updateLoadInfo} = require('../controllers/loads-controllers');
const {asyncWraper} = require('../helpers/asyncWraper.js');
const {authMiddleware} = require('../middlewares/auth-middleware');

router.get('/', authMiddleware, asyncWraper(getLoadsList));
router.post('/', authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(createLoad));
router.get('/active',
    authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(getActiveLoad));
router.patch('/active/state',
    authMiddleware,
    asyncWraper(isDriver),
    asyncWraper(updateState));
router.get('/:id', authMiddleware, asyncWraper(getLoadById));
router.put('/:id', authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(updateLoadInfo));
router.delete('/:id', authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(deleteLoad));
router.post('/:id/post',
    authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(postLoad));
router.get('/:id/shipping_info',
    authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(getShippingInfo));

module.exports = router;

