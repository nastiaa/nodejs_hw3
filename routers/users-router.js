const express = require('express');
const router = new express.Router();
const {
  getProfileInfo,
  deleteUser,
  changePassword} = require('../controllers/users-controllers');
const {isShipper} = require('../middlewares/check-role-middleware');
const {asyncWraper} = require('../helpers/asyncWraper.js');
const {authMiddleware} = require('../middlewares/auth-middleware');

router.get('/me', authMiddleware, asyncWraper(getProfileInfo));
router.patch('/me', authMiddleware, asyncWraper(changePassword));
router.delete('/me', authMiddleware,
    asyncWraper(isShipper),
    asyncWraper(deleteUser));

module.exports = router;
