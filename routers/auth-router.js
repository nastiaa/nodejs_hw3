const express = require('express');
const router = new express.Router();
const {login, register} = require('../controllers/auth-controllers');
const {asyncWraper} = require('../helpers/asyncWraper.js');
const {validateRegistration} = require('../middlewares/validation-middleware');

router.post('/register',
    asyncWraper(validateRegistration),
    asyncWraper(register));
router.post('/login', asyncWraper(login));

module.exports = router;
