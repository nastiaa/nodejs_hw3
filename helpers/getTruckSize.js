module.exports.getTruckSize = (truck) => {
  const truckSizes = {};
  if (truck.type === 'SPRINTER') {
    truckSizes.payload = 1700;
    truckSizes.width = 300;
    truckSizes.length = 250;
    truckSizes.height = 170;
  }
  if (truck.type === 'SMALL STRAIGHT') {
    truckSizes.payload = 2500;
    truckSizes.width = 500;
    truckSizes.length = 250;
    truckSizes.height = 170;
  }
  if (truck.type === 'LARGE STRAIGHT') {
    truckSizes.payload = 4000;
    truckSizes.width = 700;
    truckSizes.length = 350;
    truckSizes.height = 200;
  }

  return truckSizes;
};
