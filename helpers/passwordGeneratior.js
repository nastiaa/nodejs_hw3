module.exports.generatePassword = (minLength = 6, maxLength = 30) => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '012346789';
  const pwdLength = Math.round(Math.random() * (maxLength - minLength + 1));
  let newPassword = '';
  for (let i = 0; i < pwdLength; i++) {
    if (i % 3 === 0) {
      newPassword += numbers[Math.round(Math.random() * numbers.length)];
    }
        i%2===0? newPassword +=
        alphabet[Math.round(Math.random() * alphabet.length)]:
        newPassword +=
         alphabet[Math.round(Math.random() * alphabet.length)].toUpperCase();
  }

  return newPassword;
};
